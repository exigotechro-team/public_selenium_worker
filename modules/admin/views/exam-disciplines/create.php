<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\db\ExamDisciplines */

$this->title = Yii::t('app', 'Create Exam Disciplines');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Exam Disciplines'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="exam-disciplines-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
