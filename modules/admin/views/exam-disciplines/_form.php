<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\db\ExamDisciplines */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="exam-disciplines-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'discipline_label')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'exam_year')->textInput() ?>

    <?= $form->field($model, 'site')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
