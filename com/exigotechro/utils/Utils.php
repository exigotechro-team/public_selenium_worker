<?php

namespace com\exigotechro\utils;


class Utils
{

    public static function GetTodayCompact(){
        return date('Ymd', strtotime("now"));
    }

    public static function CheckDirExists($file_name)
    {
        $dir = dirname($file_name);
        if(!file_exists($dir)){
            @mkdir($dir, 0777, true);
        }
    }

    /**
     * @param string $nos
     * @return array|null
     */
    public static function BuildArrayFromStringParam($nos)
    {
        if(empty($nos)){
            return []; }

        if (strpos($nos, '..') !== false)
        {
            $parts = explode('..', $nos);
            return range($parts[0], $parts[1]);
        }

        if (strpos($nos, ',') !== false)
        {
            return explode(',', $nos);
        }

        return [$nos];
    }

}