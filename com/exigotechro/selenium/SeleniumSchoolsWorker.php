<?php

namespace com\exigotechro\selenium;

class SeleniumSchoolsWorker extends BaseWorker
{
    /**
     * SeleniumWorker constructor.
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param array $site_county_ids
     * @return array
     */
    public function getSchoolsMaxPageNo($site_county_ids, $url_tmpl=null)
    {
        if(empty($url_tmpl) || !isset($url_tmpl)){
            $url_tmpl = 'http://evaluare.edu.ro/Evaluare/ListaScoli.aspx?jud=%s'; }

        /** @var \RemoteWebDriver $driver */
        $driver = $this->getDriver();

        $results = [];

        foreach($site_county_ids as $county_id)
        {
            $url = sprintf($url_tmpl, $county_id);

            $driver->get($url);
            $driver->wait(3000);

            $selectValue = $driver->findElement(\WebDriverBy::cssSelector('select[id="ContentPlaceHolderBody_DropDownList2"]>option:last-child'))
                ->getAttribute('value');

            $results["county_".$county_id] = $selectValue;
        }

        return $results;
    }

    /**
     * @param integer $site_county_id
     * @param integer $pageNos
     * @param string $fetch_url
     * @param string $page_html_dir
     */
    public function getSchoolPageHtml($site_county_id, $pageNos, $fetch_url, $page_html_dir)
    {
        /** @var \RemoteWebDriver $driver */
        $driver = $this->getDriver();

        for($pageNo=1; $pageNo<=$pageNos; $pageNo++)
        {
//            $url = sprintf($fetch_url, $site_county_id,  $pageNo);

            $driver->get($fetch_url);
            $driver->wait(3000);

//            $tblBody = $driver->findElement(\WebDriverBy::cssSelector('div#ContentPlaceHolderBody_FinalDiv>table'));
            $tblBodyRows = $driver->findElements(\WebDriverBy::cssSelector('div#ContentPlaceHolderBody_FinalDiv>table>tbody>tr'));
//            $tblBodyRows = $tblBody->findElements(\WebDriverBy::xpath('.//tbody/tr'));

            array_shift($tblBodyRows);
            array_shift($tblBodyRows);

            $tbl_tmpl = "<table>\n%s</table>\n";
            $tr_tmpl = "<tr>\n%s</tr>\n";

            $cnt = count($tblBodyRows)."\n";

            $tblStr = "";

            for($i=0; $i<$cnt; $i++)
            {
                /** @var \RemoteWebElement $row */
                $row = $tblBodyRows[$i];

                $trStr = sprintf($tr_tmpl, $row->getAttribute('innerHTML'));
                $tblStr .= $trStr;
            }

            $tblStr = sprintf($tbl_tmpl, $tblStr);

            if(!true) {
                print $tblStr . "\n"; }

            $page_file_name = sprintf("%school_%s.html", $page_html_dir, $pageNo);
            if (!empty($tblStr)) {
                file_put_contents($page_file_name, $tblStr); }

        }

    }


}