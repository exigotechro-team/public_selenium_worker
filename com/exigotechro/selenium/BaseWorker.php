<?php

namespace com\exigotechro\selenium;

use ChromeOptions;
use DesiredCapabilities;
use RemoteWebDriver;

class BaseWorker
{
    private $selenium_server_url;
    protected static $UserAgent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36';


    /** @var  RemoteWebDriver $driver */
    private $driver;

    /**
     * BaseWorker constructor.
     */
    public function __construct()
    {
        if(!empty(\Yii::$app->params['selenium_server'])){
            $this->selenium_server_url = \Yii::$app->params['selenium_server'];
        }
        $capabilities = DesiredCapabilities::chrome();

        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments(["headless", "disable-gpu", "window-size=1280x1024", "no-sandbox", '--user-agent=' . self::$UserAgent ]);
        $chromeOptions->setBinary("/usr/bin/google-chrome");

        $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        $this->driver = RemoteWebDriver::create($this->selenium_server_url, $capabilities, 5000);
    }

    /**
     * @return mixed
     */
    public function getSeleniumServerUrl()
    {
        return $this->selenium_server_url;
    }


    /**
     * @return RemoteWebDriver
     */
    public function getDriver()
    {
        return $this->driver;
    }

} //  end class
