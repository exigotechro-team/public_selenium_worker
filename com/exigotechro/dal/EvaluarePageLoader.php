<?php

namespace com\exigotechro\dal;

use app\models\collections\EvaluarePageCollection;
use app\models\db\ext\School;
use app\models\db\ext\Student;
use app\models\db\Schools;
use app\models\parser\EvaluareStudent;
use app\models\pojos\ExamGrades;
use app\models\pojos\LimbaMaternaGrades;
use app\models\pojos\MatematicaGrades;
use app\models\pojos\RomanaGrades;
use Symfony\Component\DomCrawler\Crawler;
use yii\helpers\VarDumper;

class EvaluarePageLoader
{
    protected static $SITE_TAG = "evaluare";

    /** @var EvaluarePageCollection $data */
    public $data;


    /**
     * EvaluarePageLoader constructor.
     * @param EvaluarePageCollection $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }


    /**
     *
     */
    public function loadData()
    {
        /** @var EvaluareStudent $evStudent */
        foreach ($this->data as $evStudent)
        {
            /** @var Schools $school */
            $school = School::addSchool($evStudent);

            /** @var Student $student */
            $student = Student::createStudent($evStudent);

            $student->getExamGrades()->getRomana()->setDenumire("Limba Romană");
            $student->getExamGrades()->getMatematica()->setDenumire("Matematică");
            $student->save(false);

            echo "Student id: " . $student->id . "\n";
//            echo $school->id . "\t" . $evStudent->final_grade . "\t" . $evStudent->exam_year. "\n";
        }
    }

}