<?php

namespace com\exigotechro\dal;

use app\models\collections\EvaluarePageCollection;
use app\models\parser\EvaluareStudent;
use Symfony\Component\DomCrawler\Crawler;
use yii\helpers\VarDumper;

class EvaluarePageParser extends BasePageHtmlParser
{
    protected static $SITE_TAG = "evaluare";

    public $page_no;
    public $raw_html_data;

    /**
     * EvaluarePageParser constructor.
     * @param integer string $exam_year
     * @param integer $site_county_id
     */
    public function __construct($exam_year, $site_county_id)
    {
        parent::__construct(self::$SITE_TAG, $exam_year, $site_county_id);
    }

    /**
     * @param $pageNo
     * @return EvaluarePageCollection
     */
    public function parsePage($pageNo)
    {
        $this->page_no = $pageNo;
        $this->raw_html_data = $this->getPage($this->page_no);

        $crawler = new Crawler('');
        $crawler->addHtmlContent($this->raw_html_data, 'UTF-8');
        $crawler = $crawler->filter("table>tr");

        $evPageList = new EvaluarePageCollection([]);

        /** @var \DOMElement $tblRow_domElem */
        foreach ($crawler as $tblRow_domElem)
        {
            $evPage = new EvaluareStudent();
            $evPage->setCountyIdRemote($this->site_county_id);

            $evPage->exam_year = $this->exam_year;

            /** @var \DOMNodeList $tds */
            $tds = $tblRow_domElem->getElementsByTagName("td");
//            echo $tds->length ."\t";

            /** @var \DOMNode $first */
            $first = $tds[0];
            $evPage->idx = $first->textContent;

            /** @var \DOMNode $second */
            $second = $tds[1];
            $evPage->student_name = $second->textContent;

            /** @var \DOMNode $third */
            $third = $tds[2];
            $evPage->rank = $third->textContent;

            /** @var Crawler $fourth */
            $fourth = new Crawler($tds[3]);
            /** @var Crawler $a */
            $a = $fourth->filter("td>a");
            $evPage->setSchoolHref($a->attr('href'));
            $evPage->school_name = $a->text();
            $evPage->extractSchoolId();


            // Romanian Language grades

            /** @var \DOMNode $fifth */
            $fifth = $tds[4];
            $evPage->rom_grade_initial = $fifth->textContent;

            /** @var \DOMNode $sixth */
            $sixth = $tds[5];
            $evPage->rom_grade_contested = $sixth->textContent;

            /** @var \DOMNode $seventh */
            $seventh = $tds[6];
            $evPage->rom_grade_final = $seventh->textContent;


            // Math grades

            /** @var \DOMNode $fifth */
            $eigth = $tds[7];
            $evPage->math_grade_initial = $eigth->textContent;

            /** @var \DOMNode $sixth */
            $ninth = $tds[8];
            $evPage->math_grade_contested = $ninth->textContent;

            /** @var \DOMNode $seventh */
            $tenth = $tds[9];
            $evPage->math_grade_final = $tenth->textContent;


            // Maternal language grades

            /** @var \DOMNode $eleventh */
            $eleventh = $tds[10];
            $evPage->maternal_grade_name = $eleventh->textContent;

            /** @var \DOMNode $twelfth */
            $twelfth = $tds[11];
            $evPage->maternal_grade_initial = $twelfth->textContent;

            /** @var \DOMNode $thirteenth */
            $thirteenth = $tds[12];
            $evPage->maternal_grade_contested = $thirteenth->textContent;

            /** @var \DOMNode $fourteenth */
            $fourteenth = $tds[13];
            $evPage->maternal_grade_final = $fourteenth->textContent;

            // Final grade

            /** @var \DOMNode $fifteenth */
            $fifteenth = $tds[14];
            $evPage->final_grade = $fifteenth->textContent;

            $evPageList->add($evPage);

//            print "\n" . VarDumper::dumpAsString(['evpage' => $evPage->toArray(),]) . "\n";
        }

        return $evPageList;

    }


    /**
     * @return mixed
     */
    public function getPageNo()
    {
        return $this->page_no;
    }

    /**
     * @return mixed
     */
    public function getRawHtmlData()
    {
        return $this->raw_html_data;
    }


}