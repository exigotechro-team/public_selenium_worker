<?php

namespace com\exigotechro\dal;


class BasePageHtmlParser
{
    public $exam_year;
    public $site_county_id;

    public $base_dir;
    public $data_dir_tmpl;
    public $data_abs_path_tmpl;
    public $data_abs_path;
    public $file_name_tmpl;


    /**
     * BasePageHtmlParser constructor.
     * @param string $site
     * @param integer $exam_year
     * @param integer $site_county_id
     */
    public function __construct($site, $exam_year, $site_county_id)
    {
        $this->exam_year = $exam_year;
        $this->site_county_id = $site_county_id;

        $this->base_dir = \Yii::$app->basePath;
        $this->data_dir_tmpl = \Yii::$app->settings->get('page_html_dir', $site);
        $this->file_name_tmpl = \Yii::$app->settings->get('page_name_tmpl', $site);

        $this->data_abs_path_tmpl = sprintf("%s/%s", $this->base_dir, $this->data_dir_tmpl);
        $this->data_abs_path = sprintf($this->data_abs_path_tmpl, $this->exam_year, $this->site_county_id);
    }


    /**
     * @return string
     */
    public function getBaseDir()
    {
        return $this->base_dir;
    }

    /**
     * @return mixed
     */
    public function getDataDirTmpl()
    {
        return $this->data_dir_tmpl;
    }


    /**
     * @return string
     */
    public function getDataAbsPathTmpl()
    {
        return $this->data_abs_path_tmpl;
    }


    /**
     * @return mixed
     */
    public function getSiteCountyId()
    {
        return $this->site_county_id;
    }

    /**
     * @return string
     */
    public function getDataAbsPath()
    {
        return $this->data_abs_path;
    }


    /**
     * @param integer $pageNo
     * @return string
     */
    public function getPageName($pageNo)
    {
        $page_name = sprintf($this->file_name_tmpl, $pageNo);

        return sprintf("%s%s", $this->data_abs_path, $page_name);
    }


    /**
     * @param integer $pageNo
     * @return string
     */
    public function getPage($pageNo)
    {
        $page_name = $this->data_abs_path . sprintf($this->file_name_tmpl, $pageNo);

        return file_get_contents($page_name);
    }



    /**
     * @return mixed
     */
    public function getFileNameTmpl()
    {
        return $this->file_name_tmpl;
    }


}