<?php

namespace com\exigotechro\dal;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;


class SymSerializer
{
    private $encoders;
    private $normalizers;
    private $serializer;

    /**
     * SymSerTest constructor.
     */
    public function __construct()
    {
        $this->encoders = array(new JsonEncoder());
        $this->normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($this->normalizers, $this->encoders);
    }

    public function serialize($exam_grades)
    {
        $jsonContent = $this->serializer->serialize(
            $exam_grades,
            'json',
            [
                'json_encode_options' => JSON_PRETTY_PRINT,
            ]
        );

        return $jsonContent;
    }

    public function deserialize($inJsonStr, $class, $string)
    {
        $exam_grades = $this->serializer->deserialize($inJsonStr, $class, $string);
        return $exam_grades;
    }
}