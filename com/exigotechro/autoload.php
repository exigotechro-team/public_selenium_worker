<?php
$__dir = dirname(__FILE__);

require_once(__DIR__ . '/evaluare/autoload.php');
require_once(__DIR__ . '/utils/autoload.php');
require_once(__DIR__ . '/dal/autoload.php');
require_once(__DIR__ . '/selenium/autoload.php');
require_once(__DIR__ . '/queue/autoload.php');