<?php

namespace com\exigotechro\queue;

use app\models\collections\EvaluarePageCollection;
use com\exigotechro\dal\EvaluarePageLoader;
use com\exigotechro\dal\EvaluarePageParser;
use yii\helpers\VarDumper;
use yii\queue\Queue;

class CountyPageHtmlJob extends \yii\base\BaseObject implements \yii\queue\Job
{
    public $site_county_id;
    public $exam_year;
    public $site;
    public $page_nos;

    /**
     * @param Queue $queue which pushed and is handling the job
     */
    public function execute($queue)
    {
        /** @var EvaluarePageParser $evParser */
        $evParser = new EvaluarePageParser($this->exam_year, $this->site_county_id);

        foreach($this->page_nos as $page_no)
        {
            /** @var EvaluarePageCollection $evPageList */
            $evPageList = $evParser->parsePage($page_no);

//        print "\n" . VarDumper::dumpAsString(['evPageListx' => $evPageList->toString(),]) . "\n";

            /** @var EvaluarePageLoader $evPageLoader */
            $evPageLoader = new EvaluarePageLoader($evPageList);

            $evPageLoader->loadData();
        }


        if(true){
            print "\n" . VarDumper::dumpAsString([
                    'site_county_id' => $this->site_county_id,
                    'exam_year' => $this->exam_year,
                    'site' => $this->site,
                    'page_nos' => $this->page_nos,
                ]) . "\n";
        }
    }
}