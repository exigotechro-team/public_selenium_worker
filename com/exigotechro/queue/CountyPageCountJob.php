<?php

namespace com\exigotechro\queue;

use app\models\db\Counties;
use app\models\db\CountyStats;
use com\exigotechro\selenium\SeleniumWorker;
use yii\helpers\VarDumper;

class CountyPageCountJob extends \yii\base\BaseObject implements \yii\queue\Job
{
    public $site_counties;
    public $exam_year;
    public $site;

    /**
     * @param \yii\queue\Queue $queue
     */
    public function execute($queue)
    {
        /** @var SeleniumWorker $sel_worker */
        $sel_worker = new SeleniumWorker();

        /** @var array $maxPageNos */
        $maxPageNos = $sel_worker->getCountyMaxPageNo($this->site_counties);

        foreach($maxPageNos as $county_id_raw => $maxCount)
        {
            $site_county_id = str_replace("county_", "", $county_id_raw);

            $county = Counties::findOne(['site_county_id' => $site_county_id, 'site' => $this->site]);
            $county_stats = CountyStats::findOne(['db_county_id' => $county->id]);

            if(!empty($county_stats)){
                $county_stats->pagesx20 = $maxCount;
                $county_stats->save(false);
            }
        }

        if(!true){
            print VarDumper::dumpAsString([
                'site_counties' => $this->site_counties,
                'exam_year' => $this->exam_year,
                    'site' => $this->site,
                    'maxPageNos' => $maxPageNos,
            ])."\n";
        }
    }

}