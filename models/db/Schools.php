<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%schools}}".
 *
 * @property int $id
 * @property string $school_name
 * @property string $school_type
 * @property int $site_county_id
 * @property string $location_type
 * @property string $phone
 * @property string $fax
 * @property int $site_school_id
 *
 * @property NotificationRequests[] $notificationRequests
 * @property Students[] $students
 */
class Schools extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%schools}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['school_name', 'site_county_id'], 'required'],
            [['site_county_id', 'site_school_id'], 'default', 'value' => null],
            [['site_county_id', 'site_school_id'], 'integer'],
            [['school_name'], 'string', 'max' => 125],
            [['school_type'], 'string', 'max' => 56],
            [['location_type'], 'string', 'max' => 32],
            [['phone', 'fax'], 'string', 'max' => 12],
            [['site_school_id'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'school_name' => 'School Name',
            'school_type' => 'School Type',
            'site_county_id' => 'Site County ID',
            'location_type' => 'Location Type',
            'phone' => 'Phone',
            'fax' => 'Fax',
            'site_school_id' => 'Site School ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationRequests()
    {
        return $this->hasMany(NotificationRequests::className(), ['db_school_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Students::className(), ['db_school_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\SchoolsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\SchoolsQuery(get_called_class());
    }
}
