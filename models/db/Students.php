<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%students}}".
 *
 * @property int $id
 * @property string $student_name
 * @property int $site_school_id
 * @property int $exam_year
 * @property string $exam_grades
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Schools $siteSchool
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%students}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_name', 'site_school_id', 'exam_year'], 'required'],
            [['site_school_id', 'exam_year'], 'default', 'value' => null],
            [['site_school_id', 'exam_year'], 'integer'],
            [['exam_grades'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['student_name'], 'string', 'max' => 125],
            [['site_school_id'], 'exist', 'skipOnError' => true, 'targetClass' => Schools::className(), 'targetAttribute' => ['site_school_id' => 'site_school_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_name' => 'Student Name',
            'site_school_id' => 'Site School ID',
            'exam_year' => 'Exam Year',
            'exam_grades' => 'Exam Grades',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiteSchool()
    {
        return $this->hasOne(Schools::className(), ['id' => 'site_school_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\StudentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\StudentsQuery(get_called_class());
    }
}
