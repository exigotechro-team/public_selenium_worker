<?php

namespace app\models\db\ext;

use app\models\db\Students;
use app\models\parser\EvaluareStudent;
use app\models\pojos\ExamGrades;
use app\models\pojos\LimbaMaternaGrades;
use app\models\pojos\MatematicaGrades;
use app\models\pojos\RomanaGrades;
use com\exigotechro\dal\SymSerializer;

class Student extends Students
{
    /** @var ExamGrades $_exam_grades */
    private $_exam_grades;

    /** @var SymSerializer $sst */
    private $sst;

    private static $DATE_FORMAT = "Y-m-d H:i:s";

    /**
     * @param EvaluareStudent $evStudent
     * @return Student
     */
    public static function createStudent($evStudent)
    {
        /** @var Student $student */
        $student = Student::findOne([
            "(exam_grades->>'countyRank')::int" => intval($evStudent->rank),
            'site_school_id' => intval($evStudent->school_id_remote),
            'exam_year' => intval($evStudent->exam_year),
        ]);

        if(!empty($student)){
            return $student; }

        $student = new Student();
        $student->student_name = $evStudent->student_name;
        $student->site_school_id = $evStudent->school_id_remote;
        $student->exam_year = $evStudent->exam_year;

        $student->buildAndSetExamGrades($evStudent);
        $student->save(false);

        return $student;
    }


    public function rules()
    {
        return array_merge(parent::rules(), [
            [['created_at'], 'date','format' => self::$DATE_FORMAT],
        ]);
    }


    public function __construct()
    {
        parent::__construct();

        $this->sst = new SymSerializer();

        $this->_exam_grades = new ExamGrades();
        $this->exam_grades = $this->sst->serialize($this->_exam_grades);
    }


    public function beforeSave($insert)
    {
        if ($this->isNewRecord)
        {
            $this->created_at = date(self::$DATE_FORMAT);
        }
        elseif(!$this->isNewRecord)
        {
            $this->updated_at = date(self::$DATE_FORMAT);
        }

        if(empty($this->_exam_grades) && empty($this->exam_grades)){
            $this->_exam_grades = new ExamGrades(); }

        $this->exam_grades = $this->sst->serialize($this->_exam_grades);

        return parent::beforeSave($insert);
    }


    public function afterFind()
    {
        parent::afterFind();

        $this->_exam_grades = $this->sst->deserialize($this->exam_grades, ExamGrades::class, 'json');
    }


    /**
     * @return ExamGrades
     */
    public function getExamGrades()
    {
        if(!isset($this->exam_grades) || empty($this->exam_grades))
        {
            $this->_exam_grades = new ExamGrades();
            $this->exam_grades = $this->sst->serialize($this->_exam_grades);
        }

        return $this->_exam_grades;
    }


    /**
     * @param ExamGrades $exam_grades
     */
    public function setExamGrades($exam_grades)
    {
        $this->_exam_grades = $exam_grades;
        $this->exam_grades = $this->sst->serialize($this->_exam_grades);
    }

    /**
     * @param EvaluareStudent $evStudent
     */
    public function buildAndSetExamGrades($evStudent)
    {
        /** @var RomanaGrades $romGrades */
        $romGrades = new RomanaGrades(null, $evStudent->rom_grade_initial, $evStudent->rom_grade_contested, $evStudent->rom_grade_final);

        /** @var MatematicaGrades $mateGrades */
        $mateGrades = new MatematicaGrades(null, $evStudent->math_grade_initial, $evStudent->math_grade_contested, $evStudent->math_grade_final);

        /** @var LimbaMaternaGrades $lmGrades */
        $lmGrades = new LimbaMaternaGrades($evStudent->maternal_grade_name, $evStudent->maternal_grade_initial, $evStudent->maternal_grade_contested, $evStudent->maternal_grade_final);

        /** @var ExamGrades $exam_grades */
        $exam_grades = new ExamGrades();
        $exam_grades->setRomana($romGrades);
        $exam_grades->setMatematica($mateGrades);
        $exam_grades->setLimbaMaterna($lmGrades);
        $exam_grades->setMediaGenerala($evStudent->final_grade);
        $exam_grades->setCountyRank($evStudent->rank);

        $this->setExamGrades($exam_grades);
    }


}