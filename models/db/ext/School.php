<?php

namespace app\models\db\ext;

use app\models\db\Schools;
use app\models\parser\EvaluareStudent;

class School extends Schools
{

    /**
     * @param EvaluareStudent $webParsedStudent
     * @return Schools|static
     */
    public static function addSchool($webParsedStudent)
    {
        $school = Schools::findOne(['site_school_id' => $webParsedStudent->school_id_remote]);

        if(!empty($school)){
            return $school; }

        $school = new Schools();
        $school->site_school_id = intval($webParsedStudent->school_id_remote);
        $school->school_name = $webParsedStudent->school_name;
        $school->site_county_id = $webParsedStudent->county_id_remote;

        $school->save(false);

        return $school;
    }

}