<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%counties}}".
 *
 * @property int $id
 * @property int $site_county_id
 * @property string $name_en
 * @property string $name_ro
 * @property string $site
 *
 * @property CountyStats[] $countyStats
 * @property Schools[] $schools
 */
class Counties extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%counties}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['site_county_id'], 'default', 'value' => null],
            [['site_county_id'], 'integer'],
            [['name_en', 'site'], 'required'],
            [['name_en', 'name_ro'], 'string', 'max' => 56],
            [['site'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'site_county_id' => 'Site County ID',
            'name_en' => 'Name En',
            'name_ro' => 'Name Ro',
            'site' => 'Site',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountyStats()
    {
        return $this->hasMany(CountyStats::className(), ['db_county_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchools()
    {
        return $this->hasMany(Schools::className(), ['db_county_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\CountiesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\CountiesQuery(get_called_class());
    }
}
