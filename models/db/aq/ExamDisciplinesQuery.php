<?php

namespace app\models\db\aq;

/**
 * This is the ActiveQuery class for [[\app\models\db\ExamDisciplines]].
 *
 * @see \app\models\db\ExamDisciplines
 */
class ExamDisciplinesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\db\ExamDisciplines[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\ExamDisciplines|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
