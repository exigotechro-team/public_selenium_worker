<?php

namespace app\models\db\aq;

/**
 * This is the ActiveQuery class for [[\app\models\db\Students]].
 *
 * @see \app\models\db\Students
 */
class StudentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return \app\models\db\Students[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\Students|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
