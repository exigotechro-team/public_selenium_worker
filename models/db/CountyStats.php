<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%county_stats}}".
 *
 * @property int $id
 * @property int $db_county_id
 * @property int $exam_year
 * @property int $pagesx20
 * @property string $fetch_start_url
 * @property int $schools20x
 *
 * @property Counties $dbCounty
 */
class CountyStats extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%county_stats}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['db_county_id', 'exam_year'], 'required'],
            [['db_county_id', 'exam_year', 'pagesx20', 'schools20x'], 'default', 'value' => null],
            [['db_county_id', 'exam_year', 'pagesx20', 'schools20x'], 'integer'],
            [['fetch_start_url'], 'string', 'max' => 125],
            [['db_county_id'], 'exist', 'skipOnError' => true, 'targetClass' => Counties::className(), 'targetAttribute' => ['db_county_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'db_county_id' => 'Db County ID',
            'exam_year' => 'Exam Year',
            'pagesx20' => 'Pagesx20',
            'fetch_start_url' => 'Fetch Start Url',
            'schools20x' => 'Schools20x',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbCounty()
    {
        return $this->hasOne(Counties::className(), ['id' => 'db_county_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\CountyStatsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\CountyStatsQuery(get_called_class());
    }
}
