<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%notification_requests}}".
 *
 * @property int $id
 * @property string $student_name
 * @property int $db_school_id
 * @property string $recepients
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Schools $dbSchool
 * @property NotificationsMade[] $notificationsMades
 */
class NotificationRequests extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notification_requests}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['student_name', 'db_school_id'], 'required'],
            [['db_school_id'], 'default', 'value' => null],
            [['db_school_id'], 'integer'],
            [['recepients'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['student_name'], 'string', 'max' => 56],
            [['db_school_id'], 'exist', 'skipOnError' => true, 'targetClass' => Schools::className(), 'targetAttribute' => ['db_school_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'student_name' => 'Student Name',
            'db_school_id' => 'Db School ID',
            'recepients' => 'Recepients',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDbSchool()
    {
        return $this->hasOne(Schools::className(), ['id' => 'db_school_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotificationsMades()
    {
        return $this->hasMany(NotificationsMade::className(), ['notif_req_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\NotificationRequestsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\NotificationRequestsQuery(get_called_class());
    }
}
