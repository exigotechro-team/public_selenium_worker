<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%notifications_made}}".
 *
 * @property int $id
 * @property int $notif_req_id
 * @property string $time_executed
 * @property int $req_status
 * @property string $req_status_details
 *
 * @property NotificationRequests $notifReq
 */
class NotificationsMade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%notifications_made}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notif_req_id', 'time_executed'], 'required'],
            [['notif_req_id', 'req_status'], 'default', 'value' => null],
            [['notif_req_id', 'req_status'], 'integer'],
            [['time_executed'], 'safe'],
            [['req_status_details'], 'string'],
            [['notif_req_id'], 'exist', 'skipOnError' => true, 'targetClass' => NotificationRequests::className(), 'targetAttribute' => ['notif_req_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notif_req_id' => 'Notif Req ID',
            'time_executed' => 'Time Executed',
            'req_status' => 'Req Status',
            'req_status_details' => 'Req Status Details',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotifReq()
    {
        return $this->hasOne(NotificationRequests::className(), ['id' => 'notif_req_id']);
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\NotificationsMadeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\NotificationsMadeQuery(get_called_class());
    }
}
