<?php

namespace app\models\db;

use Yii;

/**
 * This is the model class for table "{{%exam_disciplines}}".
 *
 * @property int $id
 * @property string $discipline_label
 * @property int $exam_year
 * @property string $site
 */
class ExamDisciplines extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%exam_disciplines}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['exam_year'], 'default', 'value' => null],
            [['exam_year'], 'integer'],
            [['site'], 'required'],
            [['discipline_label'], 'string', 'max' => 125],
            [['site'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discipline_label' => 'Discipline Label',
            'exam_year' => 'Exam Year',
            'site' => 'Site',
        ];
    }

    /**
     * @inheritdoc
     * @return \app\models\db\aq\ExamDisciplinesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\db\aq\ExamDisciplinesQuery(get_called_class());
    }
}
