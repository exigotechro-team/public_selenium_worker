<?php

namespace app\models\serializer;

use app\models\pojos\RomanaGrades;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\SerializerAwareNormalizer;


class RomanaGradesNormalizer extends SerializerAwareNormalizer  implements NormalizerInterface, DenormalizerInterface
{
    /**
     * {@inheritdoc}
     */
    public function supportsNormalization($data, $format = null)
    {
        return is_object($data) && $data instanceof RomanaGrades;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === 'RomanaGrades';
    }

    /**
     * {@inheritdoc}
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return [
            'nota' => $object->getNota(),
            'contestatie' => $object->getContestatie(),
            'nota_finala' => $object->getNotaFinala(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function denormalize($data, $class, $format = null, array $context = array())
    {
        return new RomanaGrades($data['nota'], $data['contestatie'], $data['nota_finala']);
    }
}
{

}