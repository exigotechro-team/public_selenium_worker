<?php

namespace app\models\pojos;

use yii\base\BaseObject;
use yii\helpers\VarDumper;

class BaseGrades extends BaseObject
{
    private $denumire;
    private $nota;
    private $contestatie;
    private $nota_finala;

    public $tag;

    /**
     * BaseGrades constructor.
     * @param string $denumire
     * @param string $nota
     * @param string $contestatie
     * @param string $nota_finala
     */
    public function __construct($denumire = "-", $nota = "-", $contestatie = "-", $nota_finala = "-")
    {
        parent::__construct();

        $this->denumire     = $denumire;
        $this->nota         = $nota;
        $this->contestatie  = $contestatie;
        $this->nota_finala  = $nota_finala;
    }


    /**
     * @return mixed
     */
    public function getDenumire()
    {
        return $this->denumire;
    }

    /**
     * @param mixed $denumire
     */
    public function setDenumire($denumire = "-")
    {
        $this->denumire = (empty($denumire))? "-" : $denumire;
    }


    /**
     * @return string
     */
    public function getNota(): string
    {
        return $this->nota;
    }

    /**
     * @param string $nota
     */
    public function setNota(string $nota)
    {
        $this->nota = (empty($nota))? "-" : $nota;
    }

    /**
     * @return string
     */
    public function getContestatie(): string
    {
        return $this->contestatie;
    }

    /**
     * @param string $contestatie
     */
    public function setContestatie(string $contestatie)
    {
        $this->contestatie = (empty($contestatie)) ? "-" : $contestatie;
    }

    /**
     * @return string
     */
    public function getNotaFinala(): string
    {
        return $this->nota_finala;
    }

    /**
     * @param string $nota_finala
     */
    public function setNotaFinala(string $nota_finala)
    {
        $this->nota_finala = (empty($nota_finala)) ? "-" : $nota_finala;
    }




    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;
    }

    public function __toString()
    {
        $dmpVar = VarDumper::dumpAsString([
            'grades' => $this,
        ]);

        return sprintf("\n%s\n", $dmpVar);
    }


}