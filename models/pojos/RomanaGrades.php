<?php

namespace app\models\pojos;

class RomanaGrades extends BaseGrades
{
    public static $TAG_NAME = 'ROMANA';

    /**
     * RomanaGrades constructor.
     * @param string $denumire
     * @param string $nota
     * @param string $contestatie
     * @param string $nota_finala
     */
    public function __construct($denumire = "-", $nota = "-", $contestatie = "-", $nota_finala = "-")
    {
        parent::__construct();

        $this->setDenumire($denumire);
        $this->setNota($nota);
        $this->setContestatie($contestatie);
        $this->setNotaFinala($nota_finala);

        $this->setTag(self::$TAG_NAME);
    }
}