<?php

namespace app\models\pojos;

use yii\base\BaseObject;

class ExamGrades extends BaseObject
{
    /** @var RomanaGrades $romana  */
    private $romana;

    /** @var MatematicaGrades $matematica  */
    private $matematica;

    /** @var LimbaMaternaGrades $limba_materna  */
    private $limba_materna;

    /** @var string $media_generala  */
    private $media_generala;

    /** @var integer $county_rank  */
    private $county_rank;


    /**
     * ExamGrades constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->romana = new RomanaGrades();
        $this->matematica = new MatematicaGrades();
        $this->limba_materna = new LimbaMaternaGrades();
        $this->media_generala = "-";
        $this->county_rank = 0;
    }

    /**
     * @return RomanaGrades
     */
    public function getRomana(): RomanaGrades{
        return $this->romana;
    }

    /**
     * @param RomanaGrades|array $romana
     */
    public function setRomana($romana)
    {
        if(is_array($romana))
        {
            $this->romana = new RomanaGrades($romana["denumire"], $romana["nota"], $romana["contestatie"], $romana["notaFinala"]);
        }else{
            $this->romana = $romana;
        }
    }

    /*** @return MatematicaGrades */
    public function getMatematica(): MatematicaGrades{
        return $this->matematica;
    }

    /*** @param MatematicaGrades $matematica */
    public function setMatematica($matematica)
    {
        if(is_array($matematica))
        {
            $this->matematica = new MatematicaGrades($matematica["denumire"], $matematica["nota"], $matematica["contestatie"], $matematica["notaFinala"]);
        }else{
            $this->matematica = $matematica;
        }
    }

    /*** @return LimbaMaternaGrades*/
    public function getLimbaMaterna(): LimbaMaternaGrades{
        return $this->limba_materna;
    }

    /*** @param LimbaMaternaGrades $limba_materna*/
    public function setLimbaMaterna($limba_materna)
    {
        if(is_array($limba_materna))
        {
            $this->limba_materna = new LimbaMaternaGrades($limba_materna["denumire"], $limba_materna["nota"], $limba_materna["contestatie"], $limba_materna["notaFinala"]);
        }else{
            $this->limba_materna = $limba_materna;
        }
    }

    /**
     * @return string
     */
    public function getMediaGenerala(): string{
        return $this->media_generala;
    }

    /**
     * @param string $media
     */
    public function setMediaGenerala(string $media){
        $this->media_generala = $media;
    }

    /**
     * @return int
     */
    public function getCountyRank(): int
    {
        return $this->county_rank;
    }

    /**
     * @param int $county_rank
     * @param bool $force
     */
    public function setCountyRank(int $county_rank, bool $force = false)
    {
        if($force || ( empty($this->county_rank) || !isset($this->county_rank) ))
        {
            $this->county_rank = $county_rank;
        }
        else
        {
            echo "setting the countyRank value which already exists\n";
        }
    }



}