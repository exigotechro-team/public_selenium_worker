<?php
namespace app\models\parser;

use yii\base\Model;
use yii\helpers\VarDumper;

class EvaluareStudent extends Model
{
    public $exam_year;

    public $idx;
    public $student_name;
    public $rank;
    public $school_href;
    public $school_name;
    public $school_id_remote;
    public $county_id_remote;

    public $rom_grade_initial;
    public $rom_grade_contested;
    public $rom_grade_final;

    public $math_grade_initial;
    public $math_grade_contested;
    public $math_grade_final;

    public $maternal_grade_name;
    public $maternal_grade_initial;
    public $maternal_grade_contested;
    public $maternal_grade_final;

    public $final_grade;

    public function extractSchoolId()
    {
        if(empty($this->school_href)){
            return null; }

        $attrs = explode('?', $this->school_href)[1];
        $school_id = explode('Sc=', $attrs)[1];

        $this->school_id_remote = $school_id;
    }

    /**
     * @param mixed $school_href
     * @return EvaluareStudent
     */
    public function setSchoolHref($school_href)
    {
        $this->school_href = $school_href;
        $this->extractSchoolId();
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountyIdRemote()
    {
        return $this->county_id_remote;
    }

    /**
     * @param mixed $county_id_remote
     */
    public function setCountyIdRemote($county_id_remote)
    {
        $this->county_id_remote = $county_id_remote;
    }

    public function toString()
    {
        return "\n" . sprintf("\n
                exam_year                 => %s,
                idx                       => %s,
                student_name              => %s,
                rank                      => %s,
                school_href               => %s,
                school_name               => %s,
                school_id_remote          => %s,
                county_id_remote          => %s,
                rom_grade_initial         => %s,
                rom_grade_contested       => %s,
                rom_grade_final           => %s,
                math_grade_initial        => %s,
                math_grade_contested      => %s,
                math_grade_final          => %s,
                maternal_grade_name       => %s,
                maternal_grade_initial    => %s,
                maternal_grade_contested  => %s,
                maternal_grade_final      => %s,
                final_grade               => %s,",
                 $this->exam_year,
                 $this->idx,
                 $this->student_name,
                 $this->rank,
                 $this->school_href,
                 $this->school_name,
                 $this->school_id_remote,
                 $this->county_id_remote,
                 $this->rom_grade_initial,
                 $this->rom_grade_contested,
                 $this->rom_grade_final,
                 $this->math_grade_initial,
                 $this->math_grade_contested,
                 $this->math_grade_final,
                 $this->maternal_grade_name,
                 $this->maternal_grade_initial,
                 $this->maternal_grade_contested,
                 $this->maternal_grade_final,
                 $this->final_grade) . "\n";
    }


}