<?php

namespace app\models\collections;

use app\models\parser\EvaluareStudent;

class EvaluarePageCollection implements \Countable, \IteratorAggregate
{
    /** @var array $data */
    private $data;

    public function __construct(array $data)
    {
        foreach ($data as $item) {
            if (!$item instanceof EvaluareStudent) {
                throw new \InvalidArgumentException('All items should be of EvaluarePage class.');
            }
        }
        $this->data = $data;
    }

    public function count()
    {
        return count($this->data);
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->data);
    }

    /**
     * @param EvaluareStudent $evPage
     */
    public function add($evPage)
    {
        if (!$evPage instanceof EvaluareStudent) {
            throw new \InvalidArgumentException('All items should be of EvaluarePage class.'); }

        $this->data[] = $evPage;
    }

    /**
     * @param integer $idx
     * @return EvaluareStudent
     */
    public function get($idx)
    {
        if ( $idx < 0 || $idx >= count($this->data )) {
            throw new \InvalidArgumentException('Index out of bounds.'); }

        return $this->data[$idx];
    }

    public function toString()
    {
        $str = "";

        /** @var EvaluareStudent $student */
        foreach($this->data as $student){
            $str .= $student->toString();
        }

        return $str;
    }


}