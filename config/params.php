<?php

return [
    'adminEmail' => 'admin@evaluare.exigotechro.com',
    'infoEmail' => 'info@evaluare.exigotechro.com',
    'noreplyEmail' => 'no-reply@evaluare.exigotechro.com',
    'contact' => 'contact@evaluare.exigotechro.com',

    'selenium_server' => 'http://192.168.11.26:4444/wd/hub/',
];
