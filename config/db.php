<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=127.0.0.1;dbname=selenium_wrkr',
    'username' => 'dbuser',
    'password' => 'dbpwd',
    'charset' => 'utf8',
    'tablePrefix' => 'ev_',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];