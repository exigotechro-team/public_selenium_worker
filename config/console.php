<?php

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        'log',
        'queue', // The component registers own console commands
    ],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'settings' => [
            'class' => 'pheme\settings\Module',
            'sourceLanguage' => 'en'
        ],
        'modules' => [
            'admin' => [
                'class' => 'app\modules\admin\Module',
            ],
        ],
    ],
    'components' =>
    [
//        'cache' => [ 'class' => 'yii\caching\FileCache', ],

        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'settings' => [
            'class' => 'pheme\settings\components\Settings'
        ],

        'redis' => [
            'class' => 'yii\redis\Connection',
            'hostname' => 'localhost',
            'port' => 6379,
            'database' => 0,
        ],


        'cache' => [
            'class' => 'yii\redis\Cache',
        ],

        'queue' => [
            'class' => \yii\queue\db\Queue::class,
            'db' => 'db', // DB connection component or its config
            'tableName' => '{{%queue}}', // Table name
            'channel' => 'default', // Queue channel key
            'mutex' => [
                'class' => \yii\mutex\PgsqlMutex::class, // Mutex that used to sync queries
                'db' => 'db',
            ],
            'mutexTimeout' => 0,
        ],
    ],
    'params' => $params,

    'controllerMap' =>
    [
//        'fixture' => [  'class' => 'yii\faker\FixtureController',], // Fixture generation command line.

        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'migrationNamespaces' => [
                'yii\queue\db\migrations',
            ],
            //'migrationPath' => null, // allows to disable not namespaced migration completely
        ],
    ],


];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;