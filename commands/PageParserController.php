<?php

namespace app\commands;

use app\models\collections\EvaluarePageCollection;
use com\exigotechro\dal\EvaluarePageLoader;
use com\exigotechro\dal\EvaluarePageParser;
use com\exigotechro\queue\CountyPageHtmlJob;
use com\exigotechro\utils\Utils;
use yii\console\Controller;
use yii\helpers\VarDumper;

class PageParserController extends Controller
{
    /**
     * ./yii page-parser 2017 26 2
     *
     * @param string $site_county_id
     * @param integer $page_no
     */
    public function actionIndex($exam_year, $site_county_id, $page_no)
    {
//        \Yii::$app->settings->clearCache();

        /** @var EvaluarePageParser $evParser */
        $evParser = new EvaluarePageParser($exam_year, $site_county_id);

        /** @var EvaluarePageCollection $evPageList */
        $evPageList = $evParser->parsePage($page_no);
//        print "\n" . VarDumper::dumpAsString(['evPageListx' => $evPageList->toString(),]) . "\n";

        /** @var EvaluarePageLoader $evPageLoader */
        $evPageLoader = new EvaluarePageLoader($evPageList);

        $evPageLoader->loadData();

        if(!true){
            print "\n" . VarDumper::dumpAsString([
                    'getBaseDir'            => $evParser->getBaseDir(),
                    'getDataDirTmpl'        => $evParser->getDataDirTmpl(),
                    'getDataAbsPathTmpl'    => $evParser->getDataAbsPathTmpl(),
                    'getSiteCountyId'       => $evParser->getSiteCountyId(),
                    'getDataAbsPath'        => $evParser->getDataAbsPath(),
                    'getFileNameTmpl'        => $evParser->getFileNameTmpl(),
                ]) . "\n";
        }

    }


    /**
     *
     * ./yii page-parser/page-process-data 26 1..3 2017
     *
     * @param integer $site_county_id
     * @param string $page_nos
     * @param integer $exam_year
     *
     */
    public function actionPageProcessData($site_county_id, $page_nos, $exam_year)
    {
        $pageNos = Utils::BuildArrayFromStringParam($page_nos);

        /** @var EvaluarePageParser $evParser */
        $evParser = new EvaluarePageParser($exam_year, $site_county_id);

        foreach($pageNos as $page_no)
        {
            /** @var EvaluarePageCollection $evPageList */
            $evPageList = $evParser->parsePage($page_no);

            /** @var EvaluarePageLoader $evPageLoader */
            $evPageLoader = new EvaluarePageLoader($evPageList);

            $evPageLoader->loadData();
        }

        if(!true){
            print "\n" . VarDumper::dumpAsString([
                    'site_county_id' => $site_county_id,
                    'exam_year' => $exam_year,
                    'page_nos' => $page_nos,
                ]) . "\n";
        }
    }


    /**
     *
     * Create queue job that will allow for the
     * parsing & loading of schools and students from the
     * html files already saved on disk
     *
     * ./yii page-parser/page-parse-queue-job 2017 26 1..3
     *
     * @param integer $exam_year
     * @param integer $site_county_id
     * @param string $page_nos
     */
    public function actionPageParseQueueJob($exam_year, $site_county_id, $page_nos)
    {
        $pageNosSpace = Utils::BuildArrayFromStringParam($page_nos);

        $p = new CountyPageHtmlJob([
            'site_county_id' => intval($site_county_id),
            'exam_year' => intval($exam_year),
            'page_nos'  => $pageNosSpace,
        ]);

        \Yii::$app->queue->push($p);

        if(!true)
        {
            print "\n" . VarDumper::dumpAsString([
                    'site_county_id' => intval($site_county_id),
                    'exam_year' => intval($exam_year),
                    'page_nos'  => $page_nos,
                    'pageNos'  => $pageNosSpace,
                ]) . "\n";
        }

    }


    } // end class