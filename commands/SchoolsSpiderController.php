<?php

namespace app\commands;

use app\models\db\ext\CountyStats;
use com\exigotechro\selenium\SeleniumSchoolsWorker;
use com\exigotechro\utils\Utils;
use pheme\settings\components\Settings;
use Yii;
use yii\console\Controller;
use yii\helpers\VarDumper;


class SchoolsSpiderController extends Controller
{
    /**
     * prints out usage
     */
    public function actionIndex()
    {
        $fetch_cmd = <<<FETCH_CMD
\n\t\tCmd: \t./yii schools-spider/fetch-pages \$exam_year \$counties_str \$pages_str
\t\tExample:\t./yii schools-spider/fetch-pages 2017 1..2 1..12\n\n
FETCH_CMD;

        $process_cmd = <<<PROC_CMD
\n\t\tCmd: \t./yii schools-spider/process-pages \$exam_year \$counties_str \$pages_str
\t\tExample:\t./yii schools-spider/process-pages 2017 1..2 1..12\n\n
PROC_CMD;

        $page_counts = <<<PCNTS_STR
\n\t\tCmd: \t./yii schools-spider/fetch-page-counts \$exam_year \$counties_str
\t\tExample:\t./yii schools-spider/fetch-page-counts 2017 1..2\n\n
     

PCNTS_STR;

        print "\n" . VarDumper::dumpAsString([
                'Step 00: fetch-page-counts'    => $page_counts,
                'Step 01: fetch-pages'      => $fetch_cmd,
                'Step 02: process-pages'    => $process_cmd,
            ]) . "\n";;
    }

    /**
     * ./yii schools-spider/fetch-pages 2017 1..2 1..12
     *
     * @param integer $exam_year
     * @param string $counties_str
     */
    public function actionFetchPages($exam_year, $counties_str)
    {
        $counties = Utils::BuildArrayFromStringParam($counties_str);

        /** @var Settings $settings */
        $settings = Yii::$app->settings;
        $school_page_url_tmpl = $settings->get( 'school_page_url_tmpl', 'evaluare');


        $page_html_dir_tmpl = $settings->get('school_page_html_dir', 'evaluare');
        $page_file_tmpl      = $settings->get('school_page_name_tmpl', 'evaluare');
//        evaluare.school_page_html_dir	=>  data/%s/evaluare/input/%s/schools/
//        evaluare.school_page_name_tmpl	=>  school_%s.html

        $sel_worker = new SeleniumSchoolsWorker();

        /** @var integer $site_county_id */
        foreach($counties as $site_county_id)
        {
            $fetch_url = sprintf($school_page_url_tmpl, $site_county_id);
            $schoolPageNos = CountyStats::getSchoolMaxPages($exam_year, $site_county_id)->schools20x;
            $page_html_dir = Yii::$app->basePath . "/". sprintf($page_html_dir_tmpl, $exam_year, $site_county_id);

            print "\n" . VarDumper::dumpAsString([
                    'fetch_url' => $fetch_url,
                    'schoolPageNos' => $schoolPageNos,
                    'page_html_dir' => $page_html_dir,
                    'page_file_tmpl' => $page_file_tmpl,
                ]) . "\n";

            if(!true){
                $sel_worker->getSchoolPageHtml($site_county_id, $schoolPageNos, $fetch_url, $page_html_dir);
            }



        }

        print "\n" . VarDumper::dumpAsString([
                'counties'  => $counties,
                'school_page_url_tmpl' => $school_page_url_tmpl,
            ]) . "\n";

    }

    /**
     * ./yii schools-spider/process-pages 2017 1..2 1..12
     *
     * @param integer $exam_year
     * @param string $counties_str
     * @param string $pages_str
     */
    public function actionProcessPages($exam_year, $counties_str, $pages_str)
    {
        $counties = Utils::BuildArrayFromStringParam($counties_str);
        $pages = Utils::BuildArrayFromStringParam($pages_str);

        print "\n" . VarDumper::dumpAsString([
                'counties'  => $counties,
                'pages'     => $pages,
            ]) . "\n";
    }



    /**
     * ./yii schools-spider/fetch-page-counts 2017 1..2
     *
     * @param integer $exam_year
     * @param string $counties_str
     */
    public function actionFetchPageCounts($exam_year, $counties_str)
    {
        $counties = Utils::BuildArrayFromStringParam($counties_str);

        /** @var Settings $settings */
        $settings = Yii::$app->settings;
        $school_page_url_tmpl = $settings->get( 'school_page_url_tmpl', 'evaluare');

        $sel_worker = new SeleniumSchoolsWorker();
        /** @var array $results */
        $results = $sel_worker->getSchoolsMaxPageNo($counties, $school_page_url_tmpl);

        foreach($results as $key => $maxSchoolPages)
        {
            $site_county_id = str_replace("county_", "", $key);
            CountyStats::setSchoolMaxPages($exam_year, $site_county_id, $maxSchoolPages);
        }

        print "\n" . VarDumper::dumpAsString([
                'counties'  => $counties,
                'school_page_url_tmpl' => $school_page_url_tmpl,
            ]) . "\n";
    }
}
