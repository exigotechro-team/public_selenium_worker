<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\models\pojos\ExamGrades;
use app\models\pojos\LimbaMaternaGrades;
use app\models\pojos\MatematicaGrades;
use app\models\pojos\RomanaGrades;
use com\exigotechro\dal\SymSerializer;
use yii\console\Controller;


class HelloController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'hello world')
    {
        echo $message . "\n";

        /** @var SymSerializer $sst */
        $sst = new SymSerializer();

        echo "/** ======================================== */\n";

        $exam_grades = new ExamGrades();
        $exam_grades->setRomana( new RomanaGrades(null, "8.32", "-", "-"));
        $exam_grades->setMatematica( new MatematicaGrades( null,"8.52", "8.75", "8.75"));
        $exam_grades->setLimbaMaterna( new LimbaMaternaGrades("Limba Maghiară", "8.75", "-","-"));
        $exam_grades->setMediaGenerala("7.42");
        $exam_grades->setCountyRank(742);

        $jsonStr = $sst->serialize($exam_grades);
        echo $jsonStr."\n";


        /** @var ExamGrades $examGrades */
        $examGrades = $sst->deserialize($jsonStr, ExamGrades::class, 'json');

        echo "/** ----------------------------------------- */\n";
        echo $examGrades->getRomana() ."\n";
        echo "/** ----------------------------------------- */\n";
        echo $examGrades->getMatematica()."\n";
        echo "/** ----------------------------------------- */\n";
        echo $examGrades->getLimbaMaterna() ."\n";
        echo "/** ======================================== */\n";

        $this->checkRomanaGrades($sst);
//        $this->testMateGrades($sst);
//        $this->testLimbaMaternaGrades($sst);

    }

    /**
     * @param SymSerializer $sst
     */
    private function testMateGrades($sst): void
    {
        $mateGrades = new MatematicaGrades();
        $mateGradesJson = $sst->serialize($mateGrades);
        echo $mateGradesJson . "\n";


        /** @var RomanaGrades $mateExamGrades */
        $mateExamGrades = $sst->deserialize($mateGradesJson, MatematicaGrades::class, 'json');
        $mateExamGrades->setNota("8.20");
        echo $mateExamGrades->getNota() . "\n";
        echo $mateExamGrades->getNotaFinala() . "\n";

        echo "/** ======================================== */\n";
    }

    /**
     * @param SymSerializer $sst
     */
    private function testLimbaMaternaGrades($sst): void
    {
        $lmGrades = new LimbaMaternaGrades();
        $lmGradesJson = $sst->serialize($lmGrades);
        echo $lmGradesJson . "\n";


        /** @var LimbaMaternaGrades $lmExamGrades */
        $lmExamGrades = $sst->deserialize($lmGradesJson, LimbaMaternaGrades::class, 'json');
        $lmExamGrades->setDenumire("Limba Maghiara");
        $lmExamGrades->setNotaFinala("9.23");
        echo $lmExamGrades->getNotaFinala() . "\n";
        echo $lmExamGrades->getDenumire() . "\n";

        echo "/** ======================================== */\n";
    }

    /**
     * @param SymSerializer $sst
     */
    public function checkRomanaGrades($sst)
    {
        $romGrades = new RomanaGrades();
        $romGradesJson = $sst->serialize($romGrades);
        echo $romGradesJson . "\n";


        /** @var RomanaGrades $romExamGrades */
        $romExamGrades = $sst->deserialize($romGradesJson, RomanaGrades::class, 'json');
        $romExamGrades->setNota("8.20");
        echo $romExamGrades->getNota() . "\n";
        echo $romExamGrades->getNotaFinala() . "\n";

        echo "/** ======================================== */\n";
    }


}
