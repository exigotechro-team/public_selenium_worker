<?php

namespace app\commands;

use app\models\db\Counties;
use app\models\db\CountyStats;
use com\exigotechro\queue\CountyPageHtmlJob;
use com\exigotechro\queue\CountyPageCountJob;
use com\exigotechro\selenium\SeleniumWorker;
use com\exigotechro\utils\Utils;
use pheme\settings\models\Setting;
use Yii;
use yii\console\Controller;
use yii\helpers\VarDumper;


class SpiderController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     */
    public function actionIndex($message = 'world')
    {
        $selWorker = new SeleniumWorker();

        if(true){
            $settings = \Yii::$app->settings;
            $settings->set('evaluare.test', '------ /Evaluare/CandFromJudAlfa.aspx?Jud=%s&PageN=1');
            print $settings->get('evaluare.test') . "\n";
        }


        \Yii::$app->redis->set('mykey', 'some value2');
        echo \Yii::$app->redis->get('mykey') . "\n";

        echo sprintf("hello %s! - %s", $message, $selWorker->getSeleniumServerUrl()) . "\n";
    }


    /**
     *  ./yii spider/fetch-county-size evaluare 26 2017
     *  ./yii spider/fetch-county-size evaluare 26
     *
     * @param string $site
     * @param integer $site_county_id
     * @param integer|null $year
     */
    public function actionFetchCountySize($site, $site_county_id, $year = null)
    {
        if(empty($year)){
            $year = date('Y'); }

        $county = Counties::findOne(['site_county_id' => $site_county_id, 'site' => $site]);

        $county_stats = CountyStats::findOne(['db_county_id' => $county->id,]);

        $sel_worker = new SeleniumWorker();
        $maxPageNo = $sel_worker->getCountyMaxPageNo([$site_county_id]);

        print VarDumper::dumpAsString([
            'county'        => (!empty($county)) ? $county->toArray() : [],
            'county_stats'  => (!empty($county_stats)) ? $county_stats->toArray() : [],
            'max_page_no'   => $maxPageNo,
        ]);

    }

    /**
     *  ./yii spider/fetch-county-page evaluare 26 4 2017
     *  ./yii spider/fetch-county-page evaluare 26 5..8 2017
     *  ./yii spider/fetch-county-page evaluare 26 4
     *
     *  cd /var/www/evaluare_selenium_worker
     *  nohup ./yii spider/fetch-county-page evaluare 4 1..580 2017 &
     *  ls -la data/2017/evaluare/input/4/county_* | wc -l
     *
     * @param string $site
     * @param integer $site_county_id
     * @param string $pageNosStr
     * @param integer|null $year
     */
    public function actionFetchCountyPage($site, $site_county_id, $pageNosStr, $year = null)
    {
        if(empty($year)){
            $year = date('Y'); }

        /** @var array $page_numbers */
        $page_numbers = Utils::BuildArrayFromStringParam($pageNosStr);

        $county = Counties::findOne(['site_county_id' => $site_county_id, 'site' => $site]);

        $county_stats = CountyStats::findOne(['db_county_id' => $county->id,]);

        $settings = \Yii::$app->settings;

        $page_url_tmpl      = $settings->get('page_tmpl', 'evaluare');
        $page_html_dir_tmpl = $settings->get('page_html_dir', 'evaluare');

        $page_html_dir = Yii::$app->basePath . "/". sprintf($page_html_dir_tmpl, $year, $site_county_id);

        if(true){
            $sel_worker = new SeleniumWorker();
            $sel_worker->getCountyPageHtml($site_county_id, $page_numbers, $page_url_tmpl, $page_html_dir);
        }

        if(!true){
            print VarDumper::dumpAsString([
                'site'          => $site,
                'county'        => (!empty($county)) ? $county->toArray() : [],
                'county_stats'  => (!empty($county_stats)) ? $county_stats->toArray() : [],
//            'workerResults'   => $workerResults,
                'page_url_tmpl'         => $page_url_tmpl,
                'page_html_dir_tmpl'    => $page_html_dir_tmpl,
                'page_html_dir'         => $page_html_dir,
            ]);
        }

    }




    /**
     *  ./yii spider/update-county-stats evaluare 2017
     *  ./yii spider/update-county-stats evaluare
     *
     * @param $site
     * @param null $year
     * @throws \Exception
     */
    public function actionUpdateCountyStats($site, $year = null)
    {
        if(empty($year)){
            $year = date('Y'); }

        $all_counties = Counties::findAll(['site' => $site]);

        if(empty($all_counties)){
            throw new \Exception(sprintf('No counties exists in db for site: %s!', $site)); }

        $dbConn = \Yii::$app->getDb();
        $settings = \Yii::$app->settings;
        $fetch_url_tmpl = $settings->get('evaluare.fetch_url_tmpl');


        foreach($all_counties as $county)
        {
            $county_stats = CountyStats::findOne(['db_county_id' => $county->id]);

            if(!empty($county_stats)){
                echo sprintf("There is already an entry for db_county_id: %s / site_county_id: %s\n", $county->id, $county->site_county_id);
                continue; }

            $sql_ins_tmpl =  <<<SQLINS
\t insert into ev_county_stats (db_county_id, exam_year, pagesx20, fetch_start_url) values (%s, %s, 1, '%s');
SQLINS;

            $fetch_url = sprintf($fetch_url_tmpl, $county->site_county_id);

            $sql_cmd = sprintf($sql_ins_tmpl, $county->id, $year, $fetch_url);


            $sqlCommand = $dbConn->createCommand($sql_cmd);
            $sqlCommand->query();

            echo sprintf("%s\n", $sql_cmd);
        }
    }


    /**
     *
     * ./yii spider/page-job-add 26 evaluare 2017
     * ./yii spider/page-job-add 26 evaluare
     *
     * @param $site_county_id
     * @param integer|null $exam_year
     */
    public function actionPageJobAdd($site_county_id, $site, $exam_year = null)
    {
        if(empty($exam_year)){
            $exam_year = date('Y'); }

        $site_counties = [];

        if(strpos($site_county_id, ',') !== false){
            $site_counties = explode(',', $site_county_id);
        }else{
            $site_counties[] = $site_county_id; }

        $p = new CountyPageCountJob([
            'site_counties' => $site_counties,
            'exam_year' => $exam_year,
            'site' => $site,
        ]);

        \Yii::$app->queue->push($p);
    }

}
