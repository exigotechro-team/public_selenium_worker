<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-lg-8">
                <h2>Actions</h2>
                <ul>
                    <li><a href="/admin/exam-disciplines">Exam Disciplines</a></li>
                    <li><a href="/admin/counties">Counties</a></li>
                    <li><a href="/admin/schools">Schools</a></li>
                    <li>-------------------------</li>
                    <li><a href="/gii">Gii</a></li>
                    <li><a href="/admin/settings">Settings</a></li>
                    <li>-------------------------</li>
                    <li><a href="/admin/">Admin</a></li>

                </ul>
            </div>
            <div class="col-lg-4">
                <h2>Side Column</h2>
            </div>
        </div>

    </div>
</div>
